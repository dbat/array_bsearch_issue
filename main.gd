extends Node2D

func _ready() -> void:
	test()

func test():
	# "The custom method receives two arguments (an element from the array and the value searched for)"
	var cmp = func(arg1,arg2): print("arg1:",arg1); print("arg2:", arg2);return arg1[1]>=arg2
	var X:=[ [0, 1.0], [1, 1.5], [2, 2.0], [3, 2.5] ]
	print("When before is true")
	var index = X.bsearch_custom( 1.1, cmp, true )
	print(index)
	print()
	print("When before is false")
	# We get an error:
	#  Invalid access to property or key '1' on a base object of type 'float'.
	# Because the arguments to `cmp` have swapped!
	index = X.bsearch_custom( 1.1, cmp, false )
	print(index)
